# Rilis docker image repository

This is used for gitlab-ci-multi-runner docker images generation and maintenance. Here you can find images produced and used by rilis continous integration.

## [Issue Tracker](https://gitlab.com/rilis/rilis/issues)